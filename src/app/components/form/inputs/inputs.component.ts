import { Component, OnInit, forwardRef, Input } from '@angular/core';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import {
  NG_VALUE_ACCESSOR,
  ControlValueAccessor,
  FormGroup,
  FormControl,
} from '@angular/forms';
import { Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputsComponent),
      multi: true,
    },
  ],
})
export class InputsComponent implements ControlValueAccessor {
  nameSelected: string;
  numberSelected: string;
  numberSource: Observable<any>;
  nameSource: Observable<any>;
  typeaheadLoading: boolean;
  typeaheadNoResults: boolean;

  @Input() data;

  value: {
    number: string;
    name: string;
  };

  constructor() {
    this.numberSource = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.numberSelected);
    }).pipe(
      mergeMap(value => this.getStatesAsObservable({ value, type: 'number' }))
    );

    this.nameSource = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.nameSelected);
    }).pipe(
      mergeMap(value => this.getStatesAsObservable({ value, type: 'name' }))
    );
  }

  getStatesAsObservable(token): Observable<any> {
    const query = new RegExp(token.value, 'i');
    console.log(token);
    return of(
      this.data.filter((state: any) => {
        if (token.type === 'number') {
          return query.test(state.id);
        }

        if (token.type === 'name') {
          return query.test(state.name);
        }
      })
    );
  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
    this.value = {
      name: e.item.name,
      number: e.item.id,
    };
    this.onChanged(this.value);
  }

  onChanged: any = () => {};
  onTouched: any = () => {};

  // gets the value from the formControl
  writeValue(data: any) {
    this.value = {
      name: (data && data.name) || '',
      number: (data && data.number) || '',
    };
  }

  registerOnChange(fn: any) {
    this.onChanged = fn;
  }
  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }
}
