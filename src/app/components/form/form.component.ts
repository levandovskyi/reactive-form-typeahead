import { Component, OnInit } from '@angular/core';
import { Observable, of, Subscription } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  numberSource: Observable<any>;
  nameSource: Observable<any>;
  statesGroup: FormGroup;

  statesComplex: any[] = [
    { id: 1, name: 'Alabama', region: 'South' },
    { id: 2, name: 'Alaska', region: 'West' },
    { id: 3, name: 'Arizona', region: 'West' },
    { id: 4, name: 'Arkansas', region: 'South' },
    { id: 5, name: 'California', region: 'West' },
    { id: 6, name: 'Colorado', region: 'West' },
    { id: 7, name: 'Connecticut', region: 'Northeast' },
    { id: 8, name: 'Delaware', region: 'South' },
    { id: 9, name: 'Florida', region: 'South' },
    { id: 10, name: 'Georgia', region: 'South' },
  ];

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.statesGroup = new FormGroup({
      states: this.fb.array([this.fb.control(null)]),
    });
  }

  get statesArray() {
    return this.statesGroup.get('states') as FormArray;
  }

  addRow() {
    this.statesArray.push(this.fb.control(null));
  }

  deleteRow(index: number) {
    this.statesArray.removeAt(index);
  }
}
